CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Installation


INTRODUCTION
------------

This module is designed to encrypt/decrypt all fields of every Webform
present on the site except fieldset, file, markup and pagebreak.
 *For a full description of the module, visit the project page:
  https://www.drupal.org/project/webform_config_ease


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
   
   
CONFIGURATION
-------------
 
 * Configure user permissions in Administration » People » Permissions:

   -Administer Encrypt all webform fields configuration

 * To encrypt/decrypt all components in all the webforms of your site, go to admin/config/encryptall/global/settings and select the relevant option. 
 
